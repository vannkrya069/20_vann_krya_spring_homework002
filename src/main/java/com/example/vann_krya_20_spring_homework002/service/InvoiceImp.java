package com.example.vann_krya_20_spring_homework002.service;


import com.example.vann_krya_20_spring_homework002.controller.InvoiceAddNew;
import com.example.vann_krya_20_spring_homework002.model.Invoice;
import com.example.vann_krya_20_spring_homework002.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceImp implements InvoiceService{
    private InvoiceRepository invoiceRepository;

    @Autowired
    public void setInvoiceRepository(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(int id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public int addNewInvoice(InvoiceAddNew invoiceAddNew) {
        int InvoiceId = invoiceRepository.invoiceTable(invoiceAddNew);
        for (int productId : invoiceAddNew.getProductId()
             ) {
            invoiceRepository.invoiceDetailTable(InvoiceId, productId);
        }
        return InvoiceId;
    }

    @Override
    public void deleteInvoiceById(int id) {
        invoiceRepository.deleteInvoiceById(id);
    }
}
