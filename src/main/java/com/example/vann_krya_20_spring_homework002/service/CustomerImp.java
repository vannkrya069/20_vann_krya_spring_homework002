package com.example.vann_krya_20_spring_homework002.service;

import com.example.vann_krya_20_spring_homework002.controller.CustomerAddNew;
import com.example.vann_krya_20_spring_homework002.model.Customer;
import com.example.vann_krya_20_spring_homework002.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class CustomerImp implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer addNewCustomer(CustomerAddNew customerAddNew) {
        return customerRepository.addNewCustomer(customerAddNew);
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public void deleteCustomerById(int id) {
        customerRepository.deleteCustomerById(id);
    }

    @Override
    public Customer updateCustomerById(int id, CustomerAddNew customerAddNew) {
        return customerRepository.updateCustomerById(id, customerAddNew);
    }
}
