package com.example.vann_krya_20_spring_homework002.service;


import com.example.vann_krya_20_spring_homework002.controller.ProductAddNew;
import com.example.vann_krya_20_spring_homework002.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ProductService {
    List<Product> getAllProduct();
    Product getProductById(int id);
    Product addNewProduct(ProductAddNew productAddNew);
    Product updateProductById(int id, ProductAddNew productAddNew);
    void deleteProductById(int id);
}
