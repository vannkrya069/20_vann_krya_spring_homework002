package com.example.vann_krya_20_spring_homework002.service;

import com.example.vann_krya_20_spring_homework002.controller.CustomerAddNew;
import com.example.vann_krya_20_spring_homework002.model.Customer;

import java.util.List;

public interface CustomerService {
    Customer addNewCustomer(CustomerAddNew customerAddNew);
    List<Customer> getAllCustomer();
    Customer getCustomerById(int id);
    void deleteCustomerById(int id);
    Customer updateCustomerById(int id, CustomerAddNew customerAddNew);
}
