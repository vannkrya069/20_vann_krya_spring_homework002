package com.example.vann_krya_20_spring_homework002.service;


import com.example.vann_krya_20_spring_homework002.controller.ProductAddNew;
import com.example.vann_krya_20_spring_homework002.model.Product;
import com.example.vann_krya_20_spring_homework002.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImp implements ProductService{

    public ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product addNewProduct(ProductAddNew productAddNew) {
        return productRepository.addNewProduct(productAddNew);
    }

    @Override
    public Product updateProductById(int id, ProductAddNew productAddNew) {
        return productRepository.updateProductById(id, productAddNew);
    }

    @Override
    public void deleteProductById(int id) {
        productRepository.deleteProductById(id);
    }
}
