package com.example.vann_krya_20_spring_homework002.service;

import com.example.vann_krya_20_spring_homework002.controller.InvoiceAddNew;
import com.example.vann_krya_20_spring_homework002.model.Invoice;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();
    Invoice getInvoiceById(int id);
    int addNewInvoice(InvoiceAddNew invoiceAddNew);
    void deleteInvoiceById(int id);
}
