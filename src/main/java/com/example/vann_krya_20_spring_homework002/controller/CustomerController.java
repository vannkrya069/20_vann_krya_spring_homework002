package com.example.vann_krya_20_spring_homework002.controller;


import com.example.vann_krya_20_spring_homework002.model.Customer;
import com.example.vann_krya_20_spring_homework002.model.response.CustomerResponse;
import com.example.vann_krya_20_spring_homework002.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @PostMapping("/add-new-customer")
    public ResponseEntity<?> addNewCustomer(@RequestBody CustomerAddNew customerAddNew) {
        return ResponseEntity.ok(new CustomerResponse<Customer>(
                customerService.addNewCustomer(customerAddNew),
                "Congratulations you add new customer successfully...!",
                "true"
        ));
    }


    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable int id) {
        if(customerService.getCustomerById(id) != null) {
           return ResponseEntity.ok(new CustomerResponse<Customer>(
            customerService.getCustomerById(id),
            "Congratulations you get customer by id successfully...!",
            "true"
           ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }


    @GetMapping("/get-all-customer")
    public ResponseEntity<?> getAllCustomer() {
        if(customerService.getAllCustomer().isEmpty()){
            return new ResponseEntity<>("No customers in list...!", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new CustomerResponse<List<Customer>>(
            customerService.getAllCustomer(),
                "Congratulations you get all customers successfully...!",
                "true"
        ));
    }


    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable int id) {
        if(customerService.getCustomerById(id) != null) {
            customerService.deleteCustomerById(id);
            return ResponseEntity.ok(new CustomerResponse<Customer>(
                    null,
                    "Congratulations you delete customer by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }


    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody CustomerAddNew customerAddNew) {
        if(customerService.getCustomerById(id) != null) {
            customerService.updateCustomerById(id, customerAddNew);
            return ResponseEntity.ok(new CustomerResponse<Customer>(
                    customerService.getCustomerById(id),
                    "Congratulations you update customer by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }

}
