package com.example.vann_krya_20_spring_homework002.controller;

import com.example.vann_krya_20_spring_homework002.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceAddNew {
    private Timestamp invoiceDate;
    private int CustomerId;
    private List<Integer> productId;
}
