package com.example.vann_krya_20_spring_homework002.controller;


import com.example.vann_krya_20_spring_homework002.model.Invoice;
import com.example.vann_krya_20_spring_homework002.model.response.InvoiceResponse;
import com.example.vann_krya_20_spring_homework002.service.InvoiceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    public ResponseEntity<?> getAllInvoice() {
        if(invoiceService.getAllInvoice().isEmpty()) {
            return new ResponseEntity<>("No invoices in list...!", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new InvoiceResponse<List<Invoice>>(
                invoiceService.getAllInvoice(),
                "Congratulations you get all invoices successfully...!",
                "true"
        ));
    }


    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceById(@PathVariable int id) {
        if(invoiceService.getInvoiceById(id) != null) {
            return ResponseEntity.ok(new InvoiceResponse<Invoice>(
                    invoiceService.getInvoiceById(id),
                    "Congratulations you get invoice by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }


    @PostMapping("/add-new-invoice")
    public ResponseEntity<?> addNewInvoice(@RequestBody InvoiceAddNew invoiceAddNew) {
        int InvoiceId = invoiceService.addNewInvoice(invoiceAddNew);
            return ResponseEntity.ok(new InvoiceResponse<Invoice>(
                    invoiceService.getInvoiceById(InvoiceId),
                    "Congratulations you add new invoice successfully...!",
                    "true"
            ));
    }


    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoiceById(@PathVariable int id) {
        if(invoiceService.getInvoiceById(id) != null) {
            invoiceService.deleteInvoiceById(id);
            return ResponseEntity.ok(new InvoiceResponse<Invoice>(
                    null,
                    "Congratulations you delete invoice by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }
}
