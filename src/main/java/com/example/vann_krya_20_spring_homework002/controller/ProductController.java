package com.example.vann_krya_20_spring_homework002.controller;


import com.example.vann_krya_20_spring_homework002.model.Product;
import com.example.vann_krya_20_spring_homework002.model.response.ProductResponse;
import com.example.vann_krya_20_spring_homework002.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    public ResponseEntity<?> getAllProduct() {
        if(productService.getAllProduct().isEmpty()) {
            return new ResponseEntity<>("No products in list...!", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new ProductResponse<List<Product>>(
                productService.getAllProduct(),
                "Congratulations you get all products successfully...!",
                "true"
        ));
    }


    @GetMapping("get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable int id) {
        if(productService.getProductById(id) != null) {
            return ResponseEntity.ok(new ProductResponse<Product>(
                    productService.getProductById(id),
                    "Congratulations you get product by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }


    @PostMapping("/add-new-product")
    public ResponseEntity<?> addNewProduct(@RequestBody ProductAddNew productAddNew) {
        return ResponseEntity.ok(new ProductResponse<Product>(
                productService.addNewProduct(productAddNew),
                "Congratulations you add new product successfully...!",
                 "true"
        ));
    }


    @PutMapping("/update-product-by-id/{id}")
    public  ResponseEntity<?> updateProductById(@PathVariable int id, @RequestBody ProductAddNew productAddNew) {
        if(productService.getProductById(id) != null) {
            productService.updateProductById(id, productAddNew);
            return ResponseEntity.ok(new ProductResponse<Product>(
                    productService.getProductById(id),
                    "Congratulations you update product by id successfully...!",
                    "true"
            ));
        }
        return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("/delete-product-by-id/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable int id) {
        if(productService.getProductById(id) != null) {
            productService.deleteProductById(id);
            return ResponseEntity.ok(new ProductResponse<Product>(
                    null,
                    "Congratulations you delete product by id successfully...!",
                    "true"
            ));
        }
       return new ResponseEntity<>("Id not found...!", HttpStatus.NOT_FOUND);
    }
}
