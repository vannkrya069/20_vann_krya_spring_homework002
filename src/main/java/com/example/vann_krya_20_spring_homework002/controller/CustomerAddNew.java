package com.example.vann_krya_20_spring_homework002.controller;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerAddNew {
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
