package com.example.vann_krya_20_spring_homework002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class Invoice {
    private int invoiceId;
    private Timestamp invoiceDate;
    private Customer customer;
    private List<Product> product;
}
