package com.example.vann_krya_20_spring_homework002.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class InvoiceResponse<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private String success;
}
