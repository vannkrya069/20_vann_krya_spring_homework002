package com.example.vann_krya_20_spring_homework002.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductResponse<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T payload;
    private String message;
    private String success;
}
