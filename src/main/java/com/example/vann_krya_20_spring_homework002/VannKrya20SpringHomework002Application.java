package com.example.vann_krya_20_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VannKrya20SpringHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(VannKrya20SpringHomework002Application.class, args);
    }

}
