package com.example.vann_krya_20_spring_homework002.repository;

import com.example.vann_krya_20_spring_homework002.controller.InvoiceAddNew;
import com.example.vann_krya_20_spring_homework002.model.Invoice;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {


    //getAll
    @Select("""
            SELECT * FROM invoice_tb
            """)

    @Result(property = "customer", column = "customer_id",
            one = @One(select = "com.example.vann_krya_20_spring_homework002.repository.CustomerRepository.getCustomerById")
    )
    @Result(property ="invoiceId", column = "invoice_id")
    @Result(property = "invoiceDate", column ="invoice_date")
    @Result(property = "product" , column = "invoice_id",
            many = @Many(select = "com.example.vann_krya_20_spring_homework002.repository.ProductRepository.getALlInvoiceById")
    )
    List<Invoice> getAllInvoice();

    //getById
    @Select("""
            SELECT * FROM invoice_tb
            WHERE invoice_id = #{id}
            """)
    @Result(property = "customer", column = "customer_id",
    one = @One(select = "com.example.vann_krya_20_spring_homework002.repository.CustomerRepository.getCustomerById"))
    @Result(property ="invoiceId", column = "invoice_id")
    @Result(property = "invoiceDate", column = "invoice_date")
    @Result(property = "product", column = "invoice_id",
    many = @Many(select = "com.example.vann_krya_20_spring_homework002.repository.ProductRepository.getALlInvoiceById"))
    Invoice getInvoiceById(int id);


    //insert
    @Select("""
           INSERT INTO invoice_tb(invoice_date, customer_id)
           VALUES (#{itb.invoiceDate}, #{itb.customerId})
           RETURNING invoice_id
            """)
    int invoiceTable(@Param("itb") InvoiceAddNew invoiceAddNew);

    @Select("""
            INSERT INTO invoice_detail_tb(invoice_id, product_id)
            VALUES (#{invoiceId}, #{productId})
            """)
    Integer invoiceDetailTable(int invoiceId, int productId);

    @Delete("""
            DELETE FROM invoice_tb
            WHERE invoice_id = #{id}
            """)

    void deleteInvoiceById(int id);
}
