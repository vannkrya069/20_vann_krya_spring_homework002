package com.example.vann_krya_20_spring_homework002.repository;

import com.example.vann_krya_20_spring_homework002.controller.CustomerAddNew;
import com.example.vann_krya_20_spring_homework002.model.Customer;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Mapper
public interface CustomerRepository {

    //insert
    @Select("""
            INSERT INTO customer_tb(customer_name, customer_address, customer_phone)
            VALUES (#{customer.customerName}, #{customer.customerAddress}, #{customer.customerPhone})
            RETURNING *
            """)
    @Results(id = "customerMap", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone")
    })
    Customer addNewCustomer(@Param("customer") CustomerAddNew customerAddNew);


    //getAll
    @Select("""
            SELECT * FROM customer_tb
            """)
    @ResultMap("customerMap")
    List<Customer> getAllCustomer();


    //getById
    @Select("""
            SELECT * FROM customer_tb
            WHERE customer_id = #{id}
            """)
    @ResultMap("customerMap")
    Customer getCustomerById(int id);


    //deleteById
    @Delete("""
            DELETE FROM customer_tb
            WHERE customer_id = #{id}
            """)
    @ResultMap("customerMap")
    void deleteCustomerById(int id);


    //updateById
    @Select("""
            UPDATE customer_tb
            SET customer_name = #{customer.customerName}, customer_address = #{customer.customerAddress}, customer_phone = #{customer.customerPhone}
            WHERE customer_id = #{id}
            RETURNING *
            """)
    @ResultMap("customerMap")
    Customer updateCustomerById(int id, @Param("customer")  CustomerAddNew customerAddNew);
}
