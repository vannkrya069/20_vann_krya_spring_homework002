package com.example.vann_krya_20_spring_homework002.repository;

import com.example.vann_krya_20_spring_homework002.controller.ProductAddNew;
import com.example.vann_krya_20_spring_homework002.model.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface ProductRepository {

    @Select("""
            SELECT * FROM product_tb
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price"),
    })
    List<Product> getAllProduct();


    @Select("""
            SELECT * FROM product_tb
            WHERE product_id = #{id}
            """)
    @ResultMap("productMap")
    Product getProductById(int id);


    @Select("""
            INSERT INTO product_tb(product_name, product_price)
            VALUES (#{product.productName}, #{product.productPrice})
            RETURNING *
            """)
    @ResultMap("productMap")
    Product addNewProduct(@Param("product") ProductAddNew productAddNew);

    @Select("""
            UPDATE product_tb
            SET product_name = #{product.productName}, product_price = #{product.productPrice}
            WHERE product_id = #{id}
            """)
    @ResultMap("productMap")
    Product updateProductById(int id, @Param("product") ProductAddNew productAddNew);


    @Delete("""
            DELETE FROM product_tb
            WHERE product_id = #{id}
            """)
    @ResultMap("productMap")
    void deleteProductById(int id);

    @Select("""
         SELECT * FROM product_tb ptb
         INNER JOIN invoice_detail_tb idtb ON ptb.product_id = idtb.product_id
         WHERE invoice_id = #{id};
         """)
    @ResultMap("productMap")
    public List<Product> getALlInvoiceById(Integer id);

    @Insert("""
            INSERT INTO invoice_tb
            VALUES (#{customerId}, #{productId})
            """)
    void addNewInvoice(int customerId, int productId);
}
