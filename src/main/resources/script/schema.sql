CREATE TABLE product_tb(
    product_id serial PRIMARY KEY ,
    product_name varchar(200) NOT NULL ,
    product_price decimal NOT NULL
);

CREATE TABLE customer_tb(
    customer_id serial PRIMARY KEY ,
    customer_name varchar(200) NOT NULL ,
    customer_address varchar(200) NOT NULL ,
    customer_phone varchar(20) NOT NULL
);

CREATE TABLE invoice_tb (
    invoice_id serial PRIMARY KEY ,
    invoice_date date NOT NULL ,
    customer_id int NOT NULL ,
    FOREIGN KEY (customer_id) REFERENCES customer_tb (customer_id) ON DELETE CASCADE
);

ALTER TABLE invoice_tb
    ALTER COLUMN invoice_date TYPE timestamp;


CREATE TABLE invoice_detail_tb (
    id serial PRIMARY KEY ,
    invoice_id int NOT NULL ,
    product_id int NOT NULL ,
    FOREIGN KEY (invoice_id) REFERENCES invoice_tb (invoice_id) ON DELETE CASCADE ,
    FOREIGN KEY (product_id) REFERENCES product_tb (product_id) ON DELETE CASCADE
);
